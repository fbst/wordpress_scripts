# Wordpress_Scripts



## Getting started

To make it easy for you to get started with these scripts.
You just have to follow these few steps to be able to deploy your wordpress instances with some good practices found
over the internet.
A system of backup will also be configured and will backup your database and the logs of your servers.
That script has bee tested within Ubuntu 22.04 on OVH VPS machine.

## Deploy your wordpress instance

- [ ] Connect to your future wordpress server (in my case it's an OVH VPS Server installed with Ubuntu 22.04)
- [ ] ```sudo -i``` (if you can be root, if not just add sudo before all commands)
- [ ] ```mkdir /etc/backups/ && cd /etc/backups```
- [ ] ```git clone https://gitlab.com/fbst/wordpress_scripts.git```
- [ ] ```cd wordpress_scripts```
- [ ] ```chmod +x *.sh```
- [ ] ```./install_wordpress.sh```

After these steps, the script will ask you for some informations:
- Database Name => this is the name of the database created for the wordpress
- Database User => this is the name of the user who will connect locally to the database
- WP username => this is the default admin of your wordpress instance (it's better to not chosse admin)
- WP password => this is the password of your wordpress admin given just before
- WP Email => this is the email adress of the admin
- WP URL => this is the URL that you want configure (you just have to put for example www.domainename.com) 
The SSL Certificate will be generated for that domain name, it has to be created first on your DNS configuration.
- Site Title => This is the title of your website, it's for the automatic configuration of your wordpress instance.
- Language => This is the language used for your wordpress instance
- Backup Retention time (in days) => This is the retention number of days for the backups.

## Backup configuration

This script will also deploy automatically 2 scripts and add a cron job to execute these scripts every days.
The first script will backup the database of your wordpress instance and all the files related to your instance.
The second script will backup all the logs files related to your instance. But it will also include some system logs file in case of hack for example, it should be interesting to get the logs for example for the forensics.
The number choosen in the parameter of the installation scrippt (backup retention time) will tell to the script to keep only the last 5 backups.

***