#!/usr/bin/env bash
# v1.1.0
# Author: @a

# v.1.1.0
# - Test if user is root
# - Remove sudo

set -Eeuo pipefail

msg() {
  echo >&2 -e "${1-}"
}

if [ "$EUID" -ne 0 ]
then 
  echo "Merci d'exécuter ce script en tant que root ('su' pour changer d'utilisateur)"
  exit
fi

dbPwd=
copyExistingWp=
wpToCopyPath=
wpName=
wpAdminName=
wpAdminEmail=
wpAdminPwd=
wpNumberStart=
wpNumberEnd=
isNumber='^[0-9]+$'
isEmail="^(([A-Za-z0-9]+((\.|\-|\_|\+)?[A-Za-z0-9]?)*[A-Za-z0-9]+)|[A-Za-z0-9]+)@(([A-Za-z0-9]+)+((\.|\-|\_)?([A-Za-z0-9]+)+)*)+\.([A-Za-z]{2,})+$"
ip4=$(/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)

msg "========== Informations nécessaires =========="
read -s -p "Mot de passe de la base de données (et du VPS par défaut) : " dbPwd
read -p $'\nFaire la copie d\'un WordPress déjà installé (y/n) ? ' copyExistingWp

if  [ "$copyExistingWp" = "Y" ] || [ "$copyExistingWp" = "y" ] 
then
  while [ -z "$wpToCopyPath" ] 
  do
    read -p "Le chemin du WordPress à copier (ex. /var/www/html/wordpress) : " wpToCopyPath

    if  [ -z "$wpToCopyPath" ] 
    then
      msg "Le chemin est requis, merci de réessayer"
    fi
  done
else
  while [ -z "$wpAdminName" ] 
  do
    read -p "Le nom de l'utilisateur admin à créer sur les WordPress : " wpAdminName
    if  [ -z "$wpAdminName" ] 
    then
      msg "Le nom est requis, merci de réessayer"
    fi
  done

  while [ -z "$wpAdminEmail" ] || ! [[ $wpAdminEmail =~ $isEmail ]]
  do
    read -p "L'email de l'utilisateur admin : " wpAdminEmail
    if  [ -z "$wpAdminEmail" ] || ! [[ $wpAdminEmail =~ $isEmail ]]
    then
      msg "Un email au bon format est requis, merci de réessayer"
    fi
  done

  while [ -z "$wpAdminPwd" ] 
  do
    read -p "Le mot de passe l'utilisateur admin : " wpAdminPwd
    if  [ -z "$wpAdminPwd" ] 
    then
      msg "Le mot de passe est requis, merci de réessayer"
    fi
  done
fi

while [ -z "$wpName" ] 
do
  read -p "Le nom des WordPress à créer, il sera utilisé comme nom de dossier sur le serveur et comme nom du site (ex. wordpress) : " wpName
  if  [ -z "$wpName" ] 
  then
    msg "Le nom est requis, merci de réessayer"
  fi
done

while [ -z "$wpNumberStart" ] || ! [[ $wpNumberStart =~ $isNumber ]]
do
  read -p "Le nombre de départ du compteur (10 pour que le premier WordPress créé soit wordpress10 par ex.) : " wpNumberStart
  if  [ -z "$wpNumberStart" ] || ! [[ $wpNumberStart =~ $isNumber ]]
  then
    msg "Un nombre est requis, merci de réessayer"
  fi
done

while [ -z "$wpNumberEnd" ] || ! [[ $wpNumberEnd =~ $isNumber ]] || [ "$wpNumberStart" -gt "$wpNumberEnd" ]
do
  read -p "Le nombre de fin du compteur (100 pour que le dernier WordPress créé soit wordpress100 par ex.) : " wpNumberEnd
  if  [ -z "$wpNumberEnd" ] || ! [[ $wpNumberEnd =~ $isNumber ]]
  then
    msg "Un nombre est requis, merci de réessayer"
  else
    if  [ "$wpNumberStart" -gt "$wpNumberEnd" ]
    then
      msg "Le nombre doit être plus grand que le départ du compteur, merci de réessayer"
    fi
  fi
done

msg "----- Move to /tmp folder"
cd /tmp

msg ""
msg "========== Installation/mise à jour de WP CLI =========="
result=$(curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar)
chmod +x wp-cli.phar
mv wp-cli.phar /usr/local/bin/wp
wp --info

if ! dpkg -l phpmyadmin | grep -q ^ii; then
  msg ""
  msg "========== Installation de phpmyadmin =========="
  debconf-set-selections <<<'phpmyadmin phpmyadmin/dbconfig-install boolean false'
  debconf-set-selections <<<'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'
  apt install -yq phpmyadmin

    msg ""
    msg "----- Création de la configuration apache"
    touch /etc/apache2/sites-available/phpmyadmin.conf
  cat << EOF > /etc/apache2/sites-available/phpmyadmin.conf
<Directory /var/www/html/phpmyadmin/>
  AllowOverride All
</Directory>
EOF
  a2ensite phpmyadmin
fi

msg ""
msg "========== Installation/mise à jour des packets PHP habituels =========="
apt install -y php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip unrar

if ! [ "$copyExistingWp" = "Y" ] && ! [ "$copyExistingWp" = "y" ] 
then
  wpToCopyPath="/var/www/html/$wpName"

  msg ""
  msg "========== Création d'une instance WordPress de base =========="
  msg "----- Téléchargement de WordPress"
  wp core download --path=$wpToCopyPath --skip-content --allow-root --force
  mkdir -p $wpToCopyPath/wp-content/upgrade;
  mkdir -p $wpToCopyPath/wp-content/themes;
  mkdir -p $wpToCopyPath/wp-content/plugins;

  msg ""
  msg "----- Mise en place de la config (wp-config.php)"
  wp config create --allow-root --force --path=$wpToCopyPath --dbname=$wpName --dbuser=root --dbpass=$dbPwd

  msg ""
  msg "----- Création de la base de données"
  wp db create --allow-root --path=$wpToCopyPath

  msg ""
  msg "----- Installation"
  wp core install --allow-root --path=$wpToCopyPath --url="http://$ip4/$wpName" --title=$wpName --admin_user=$wpAdminName --admin_password=$wpAdminPwd --admin_email="$wpAdminEmail"

  msg ""
  msg "----- Suppression des pages et posts par défaut"
  wp post delete $(wp post list --post_type=page,post --format=ids --allow-root --path=$wpToCopyPath) --allow-root --path=$wpToCopyPath --force

  msg ""
  msg "----- Structure des permaliens positionnée sur le mode 'Titre de la publication' "
  wp rewrite structure "/%postname%/" --allow-root --hard --path=$wpToCopyPath
  wp rewrite flush --allow-root --hard --path=$wpToCopyPath

  msg ""
  msg "----- Installation du thème"
  wp theme install astra --allow-root --activate --path=$wpToCopyPath

  msg ""
  msg "----- Installation des plugins"
  wp plugin install wp-all-import woocommerce --allow-root --activate --path=$wpToCopyPath

  msg ""
  msg "----- Passage de la propriété des fichiers à l'utilisateur apache (www-data)"
  chown -R www-data:www-data $wpToCopyPath;

  msg ""
  msg "----- Création de la configuration apache"
  touch /etc/apache2/sites-available/$wpName.conf
  cat << EOF > /etc/apache2/sites-available/$wpName.conf
<Directory /var/www/html/$wpName/>
  AllowOverride All
</Directory>
EOF
  a2ensite $wpName
  a2enmod rewrite
fi

wpToCopyName=$(basename $wpToCopyPath)
wp db export "$wpToCopyName.sql" --allow-root --path=$wpToCopyPath

msg ""
msg "========== Copie des WordPress =========="
for (( c=$wpNumberStart; c<=$wpNumberEnd; c++ ))
do 
  wpDestPath=/var/www/html/$wpName$c

  msg ""
  msg "----- Copie des fichiers de $wpToCopyPath vers $wpDestPath"
  rsync -a --info=progress2 $wpToCopyPath/* $wpDestPath --exclude themes --exclude plugins

  msg ""
  msg "----- Création des liens symboliques"
  ln -s $wpToCopyPath/wp-content/plugins/ $wpDestPath/wp-content/;
  ln -s $wpToCopyPath/wp-content/themes/ $wpDestPath/wp-content/;

  msg ""
  msg "----- Passage de la propriété des fichiers à l'utilisateur apache (www-data)"
  chown -R www-data:www-data $wpDestPath

  msg ""
  msg "----- Mise à jour de la configuration (wp-config.php)"
  sed -i "s/$wpToCopyName/$wpName$c/g" $wpDestPath/wp-config.php;
  
  msg ""
  msg "----- Création de la configuration apache"
  cp /etc/apache2/sites-available/$wpToCopyName.conf /etc/apache2/sites-available/$wpName$c.conf;
  sed -i "s/$wpToCopyName/$wpName$c/g" /etc/apache2/sites-available/$wpName$c.conf;
  a2ensite $wpName$c

  msg ""
  msg "----- Création et copie de la base de données"
  mysql -u root --password=$dbPwd -e "CREATE DATABASE $wpName$c CHARACTER SET utf8"
  wp db import "$wpToCopyName.sql" --allow-root --path=$wpDestPath
  wp db query "UPDATE wp_options SET option_value = 'http://$ip4/$wpName$c/' WHERE option_id = 1 OR option_id = 2" --allow-root --path=$wpDestPath
done

msg ""
msg "----- Redémarrage d'apache"
/etc/init.d/apache2 restart
rm "$wpToCopyName.sql"
