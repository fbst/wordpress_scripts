#!/bin/bash

##########

backup_dir=
logfiles=(
   "/var/log/messages"
   "/var/log/syslog"
   "/var/log/crowdsec.log"
   "/var/log/auth.log"
   "/var/log/mysql/error.log"
   "/var/log/apache2/access.log"
   "/var/log/apache2/error.log"
   )
retention_days=

##########

date=`date +%d-%m-%y`
path="$backup_dir/$date"
mkdir -p $path > /dev/null 2>&1
if [ $? -eq 0 ]; then
	 echo "- Successfully created directory $path"
         for log in "${logfiles[@]}"
	 do
	     cp $log $path
	     tar -cjf $backup_dir/logs_$date.tar.bz2 $path > /dev/null 2>&1 
	 done
	 rm -rf $path
	 old_date=`date --date="$retention_days day ago" +%d-%m-%y`
	 old_path="$backup_dir/$old_date"
	 ls $old_path > /dev/null 2>&1
	 if [ $? -eq 0 ]; then
	    rm -rf $old_path > /dev/null 2>&1
	    if [ $? -eq 0]; then
		   echo "- Successfully removed old backup on $old_date"
	    else
		   echo "- Failed to remove old backup $old_path" && exit 1
		fi
	fi
fi
