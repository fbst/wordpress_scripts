#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

# Download wordpress cli
sudo curl -s -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
sudo chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
backup_dir_wordpress="/opt/backup_wordpress/"
backup_dir_logs="/opt/backup_logs/"
# accept user input
echo "Database Name: "
read -e dbname

echo "Database User: "
read -e dbuser

echo "Database Password: "
read -e -s dbpassword

echo "WP username: "
read -e wpuser

echo "WP Password: "
read -e -s wppassword

echo "WP email: "
read -e EMAIL

echo "WP URL: "
read -e SITEURL

# accept the name of our website
echo "Site Title: "
read -e wptitle

# accept the name of our website
echo "Language (e.g: fr_FR ): "
read -e wplang


echo "Backup retention time (in days): "
read -e retentiontime

echo "Do you want to enable firewall: only ports 22,80 and 443 will be opened ? (yes/no) "
read -e firewallconfig

echo "Do you want to install crowdsec? (yes/no) "
read -e crowsecinstall

sudo sed -i "/#\$nrconf{restart} = 'i';/s/.*/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf
sudo NEEDRESTART_MODE=a apt-get -qq update 1> /dev/null
sudo NEEDRESTART_MODE=a apt-get -qq -y upgrade 1> /dev/null
sudo NEEDRESTART_MODE=a apt-get -qq -y install fail2ban ufw mariadb-server certbot expect sendmail php-soap php-fpm php-curl php-intl php-mbstring php-gd php-zip php-xml php-mysql php-imap php-imagick php-redis nginx-extras 1> /dev/null

echo "System update and dependencies installation: OK"
SECURE_MYSQL=$(sudo expect -c "
set timeout 10
spawn mysql_secure_installation

expect \"Enter current password for root (enter for none): \"
send \"n\r\"
expect \"Switch to unix_socket authentication \[Y/n\] \"
send \"n\r\"
expect \"Change the root password? \[Y/n\] \"
send \"n\r\"
expect \"Remove anonymous users? \[Y/n\] \"
send \"y\r\"
expect \"Disallow root login remotely? \[Y/n\] \"
send \"y\r\"
expect \"Remove test database and access to it? \[Y/n\] \"
send \"y\r\"
expect \"Reload privilege tables now? \[Y/n\] \"
send \"y\r\"
expect eof
")
sudo mysql -e "CREATE DATABASE IF NOT EXISTS $dbname CHARACTER SET utf8 COLLATE utf8_unicode_ci; CREATE USER '$dbuser'@'localhost' IDENTIFIED BY '$dbpassword'; GRANT ALL PRIVILEGES ON $dbname.* TO '$dbuser'@'localhost';FLUSH PRIVILEGES;"
echo "Mysql DB configuration for wordpress instance: OK"
sudo mkdir -p /var/www/users/wor /var/www/users/wor/home /var/www/users/wor/${SITEURL} /var/www/users/wor/${SITEURL}/app /var/www/users/wor/${SITEURL}/data /var/www/users/wor/${SITEURL}/scripts /var/www/users/wor/${SITEURL}/log /var/www/users/wor/${SITEURL}/tmp
sudo useradd wor --home /var/www/users/wor/home --gid users --shell /bin/bash
sudo chmod 0751 /var/www/users /var/www/users/wor /var/www/users/wor/home /var/www/users/wor/${SITEURL} /var/www/users/wor/${SITEURL}/app
sudo chmod 0701 /var/www/users/wor/${SITEURL}/tmp
sudo chmod 0710 /var/www/users/wor/${SITEURL}/data
sudo chmod 0700 /var/www/users/wor/${SITEURL}/scripts /var/www/users/wor/${SITEURL}/log
sudo chgrp users /var/www/users/wor /var/www/users/wor/${SITEURL}
sudo chown wor:users /var/www/users/wor/home /var/www/users/wor/${SITEURL}/app /var/www/users/wor/${SITEURL}/log /var/www/users/wor/${SITEURL}/tmp /var/www/users/wor/${SITEURL}/scripts
sudo chown wor:www-data /var/www/users/wor/${SITEURL}/data
echo "Configuration of your system: OK"
runuser -l wor -c "wget -q https://fr.wordpress.org/wordpress-latest-fr_FR.tar.gz"
runuser -l wor -c "tar xvzf wordpress-latest-fr_FR.tar.gz -C /var/www/users/wor/${SITEURL}/app 1> /dev/null"
sudo ln -s /var/www/users/wor/${SITEURL}/app/wordpress /var/www/users/wor/${SITEURL}/web
sudo touch /etc/php/8.1/fpm/pool.d/wordpress.conf

### Fail2ban Configuration
sudo touch /etc/fail2ban/filter.d/wordpress.conf
sudo touch /etc/fail2ban/jail.d/wordpress.conf
sudo echo "# We check for any POST request attempts on the login page or the XML RPC API entry point. Note: it would be even better to entirely disable xmlrpc if you don't use Jetpack and the likes.

[Definition]
      failregex = ^<HOST> .* "POST .*wp-login.php
                  ^<HOST> .* "POST .*xmlrpc.php
      ignoreregex =" > /etc/fail2ban/filter.d/wordpress.conf

sudo echo "[wordpress]
      enabled = true #explicit, isn't it?
      port = http,https #check both protocols
      filter = wordpress #use the filter we defined above
      logpath = /var/log/nginx/access.log #the log to apply the filter against
      maxretry = 10 #how many times we allow the filter to find a match....
      findtime = 600 #...over the last 10 minutes (10x60seconds) ...
      banaction = ufw #...before we ban this IP with UFW.... UFW is much simpler than iptables, but as powerful (as it uses iptables underneath)
      ignoreip = #...unless it comes from this IP (aka: yours)" > /etc/fail2ban/jail.d/wordpress.conf


### php configuration hardening
sudo touch /etc/php/8.1/fpm/conf.d/99-hardening.ini
sudo echo "#Dangerous functions that should be disabled on a regular WP install
disable_functions = exec,passthru,shell_exec,system,proc_open,popen,show_source,syslog,openlog

#Don't expose PHP version, don't display errors, log them
expose_php = Off
display_errors = Off
display_startup_errors = Off
#Log everything except deprectate and strict on a production server.
error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
log_errors = On

# Custom PHP settings for WordPress and WooCommerce
# Allow a maximum POST size of 25MB.
post_max_size = 25M
# Allow a maximum size file of 25MB
upload_max_filesize = 25M
# Allow up to 5 files to be uploaded in one request
max_file_uploads = 5
#Make sure everything is in UTF-8 to avoid problems.
default_charset = "UTF-8"
#A PHP script can't consume more 256MB of ram...
memory_limit = 256M
#...for up to 30 seconds...
max_execution_time = 30
# The worker will wait for 60 seconds to receive the complete request before closing.
max_input_time = 60" > /etc/php/8.1/fpm/conf.d/99-hardening.ini
sudo systemctl restart php8.1-fpm.service


echo "Downloading latest version of wordpress and deployement: OK"
runuser -l wor -c "wp core config --path=/var/www/users/wor/${SITEURL}/app/wordpress/ --dbname=$dbname --dbuser=$dbuser --dbpass=$dbpassword --extra-php <<PHP
define('AUTOSAVE_INTERVAL', 300 );
define('WP_POST_REVISIONS', false );
define( 'WP_AUTO_UPDATE_CORE', true );
define( 'WP_DEBUG', false );
PHP" 
runuser -l wor -c "wp core install --path=/var/www/users/wor/${SITEURL}/app/wordpress/ --url='$SITEURL' --title='$wptitle' --admin_user='$wpuser' --admin_password='$wppassword' --admin_email='$EMAIL'"
echo "WP-Config.php generated and first installation: OK"
echo "[wordpress]
user = wor
group = users
listen = /var/run/php7-fpm-wor.sock
listen.owner = www-data
listen.group = www-data
listen.mode = 0666
pm = dynamic
pm.max_children = 10
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 5
pm.max_requests = 1000
catch_workers_output = yes
security.limit_extensions = .php
php_value[include_path] = .
php_admin_value[error_log] = /var/www/users/wor/SITEURL/log/php-fpm.error.log
php_admin_value[max_input_time] = 180
php_admin_value[max_input_vars] = 3000
php_admin_value[memory_limit] = 256M
php_admin_value[open_basedir] = /var/www/users/wor/SITEURL/web/:/var/www/users/wor/SITEURL/app/:/var/www/users/wor/SITEURL/data/:/var/www/users/wor/SITEURL/log/:/var/www/users/wor/SITEURL/tmp/
php_admin_value[upload_tmp_dir] = /var/www/users/wor/SITEURL/tmp/
" >> /etc/php/8.1/fpm/pool.d/wordpress.conf
if [ $crowdsecinstall == 'yes' ]; then
   sudo curl -s https://packagecloud.io/install/repositories/crowdsec/crowdsec/script.deb.sh | sudo bash 1>/dev/null
   sudo NEEDRESTART_MODE=a apt-get -qq -y install crowdsec 1>/dev/null
   runuser -l wor -c "wp plugin install crowdsec --activate --path='/var/www/users/word/{$SITEURL}/app/wordpress/"
   sudo cscli hub update 2>/dev/null
   sudo cscli hub upgrade 2>/dev/null
   crowdsec_bouncer_key=`sudo cscli bouncers add wordpress-bouncer`
fi


sudo sed -i "s/SITEURL/${SITEURL}/g" /etc/php/8.1/fpm/pool.d/wordpress.conf
sudo systemctl restart php8.1-fpm 1> /dev/null
echo "Creating wp-config.php and Configuration of your wordpress instance: OK"
sudo certbot certonly --standalone --agree-tos --no-eff-email -m ${EMAIL} -d ${SITEURL} --pre-hook "systemctl stop nginx" --post-hook "systemctl start nginx" 1> /dev/null
sudo rm /etc/nginx/sites-enabled/default
sudo mkdir /etc/nginx/includes
sudo mkdir -p /var/acme/.well-known/acme-challenge/
sudo touch /etc/nginx/includes/acme.conf
sudo echo "
location ^~ /.well-known/acme-challenge/ {
    root      /var/acme;

    autoindex off;
    index     index.html;
    try_files $uri $uri/ =404;
    auth_basic off;
}
">>/etc/nginx/includes/acme.conf
sudo touch /etc/nginx/sites-available/${SITEURL}.conf
sudo echo "
server {
  listen *:80;
  server_name      SITEURL;

  index            index.html index.php;
  access_log       /var/log/nginx/SITEURL.access.log combined;
  error_log        /var/log/nginx/SITEURL.error.log;

  location / {
    index     index.html index.php;
    rewrite ^ https://$server_name$request_uri? permanent;
  }
  include includes/acme.conf;
}
">>/etc/nginx/sites-available/${SITEURL}.conf
sudo touch /etc/nginx/sites-available/${SITEURL}_ssl.conf
sudo echo "
server {
  listen       *:443 ssl http2;
  listen       [::]:443 ssl http2 ;
  server_name  SITEURL;

  ssl_certificate           /etc/letsencrypt/live/SITEURL/fullchain.pem;
  ssl_certificate_key       /etc/letsencrypt/live/SITEURL/privkey.pem;
  ssl_session_cache         shared:SSL:20m;
  ssl_session_timeout       1d;
  ssl_session_tickets       off;
  ssl_buffer_size           4k;
  ssl_protocols             TLSv1.2 TLSv1.3;
  ssl_ciphers               ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA;
  ssl_prefer_server_ciphers off;
  ssl_stapling              on;
  ssl_stapling_verify       on;

  index  index.html index.php;
  access_log            /var/log/nginx/ssl-SITEURL_ssl.access.log combined;
  error_log             /var/log/nginx/ssl-SITEURL_ssl.error.log;
  root /var/www/users/wor/SITEURL/web/;


  location ~ (.+\.php)(.*)$ {
    include       /etc/nginx/fastcgi.conf;

    fastcgi_pass  unix:/var/run/php7-fpm-wor.sock;
    fastcgi_index index.php;
    fastcgi_split_path_info ^(.+\.php)(.*)$;
    fastcgi_param PATH_INFO \$fastcgi_path_info;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    fastcgi_hide_header X-Frame-Options;
    fastcgi_intercept_errors on;
  }
}
">>/etc/nginx/sites-available/${SITEURL}_ssl.conf

sudo sed -i "s/SITEURL/${SITEURL}/g" /etc/nginx/sites-available/*
cd /etc/nginx/sites-enabled
sudo ln -s ../sites-available/${SITEURL}.conf
sudo ln -s ../sites-available/${SITEURL}_ssl.conf
sudo systemctl restart nginx
echo "SSL Certificate generation and configuration for your wordpress instance: OK"
sudo mkdir /etc/backup_wordpress
sudo mkdir /etc/backup_logs
mv /etc/backups/wordpress_scripts/backup_logs.sh /etc/backup_logs/backup_logs.sh
mv /etc/backups/wordpress_scripts/backup_wordpress.sh /etc/backup_wordpress/backup_wordpress.sh
sudo sed -i "s/backup_dir=/backup_dir=\"\/opt\/backup_wordpress\/\"/g" /etc/backup_wordpress/backup_wordpress.sh
sudo sed -i "s/backup_dir=/backup_dir=\"\/opt\/backup_logs\/\"/g" /etc/backup_logs/backup_logs.sh
sudo sed -i "s/webapp_path=/webapp_path=\"\/var\/www\/users\/wor\/${SITEURL}\/\"/g" /etc/backup_wordpress/backup_wordpress.sh
sudo sed -i "s/database_name=/database_name=\"$dbname\"/g" /etc/backup_wordpress/backup_wordpress.sh
sudo sed -i "s/database_user=/database_user=\"$dbuser\"/g" /etc/backup_wordpress/backup_wordpress.sh
sudo sed -i "s/retention_days=/retention_days=$retentiontime/g" /etc/backup_wordpress/backup_wordpress.sh
sudo sed -i "s/retention_days=/retention_days=$retentiontime/g" /etc/backup_logs/backup_logs.sh
sudo crontab -l | { cat; echo "0 */2 * * * wp core update --major --path=/var/www/users/wor/${SITEURL}/app/wordpress/"}
sudo crontab -l | { cat; echo "0 3 * * 1 wp  plugin update --all --path=/var/www/users/wor/${SITEURL}/app/wordpress/"}
sudo crontab -l | { cat; echo "0 4 * * 7 /usr/local/bin/wp cli update"; } | crontab - 1>/dev/null
sudo crontab -l | { cat; echo "30 23 * * * /etc/backup_wordpress/backup_wordpress.sh"; } | crontab - 1> /dev/null
sudo crontab -l | { cat; echo "31 23 * * * /etc/backup_logs/backup_logs.sh"; } | crontab - 1> /dev/null
echo "Backup Configuration: OK"
if [ $firewallconfig == 'yes' ]; then
   sudo ufw default deny incoming
   sudo ufw default allow outgoing
   sudo ufw allow ssh
   sudo ufw allow https
   yes | sudo ufw enable
   echo "Firewall Configuration: OK"
fi
echo 
echo "================================================================="
echo "Installation completed successfully."
echo ""
echo "Username WP: $wpuser"
echo "Password WP: $wppassword"
if [ $crowdsecinstall == 'yes' ]; then
echo "Crowdsec Bouncer key: `echo $crowdsec_bouncer_key | pcregrep -o1 -e ".*:\s(\w+)"`"
fi
echo "URL WP: https://$SITEURL"
echo ""
echo "================================================================="